Hippo
========================
Instructions to run the app.

```
npm install
npm start
```

Twitter/Facebook/Google auth is enabled by default, but you can easily turn it off  by commenting out the `passport.use()` statements in the [server.js](server.js) file.
If you want to enable any of the social logins make sure to set the appropriate environment variables:

| Provider | Key | Default value |
| ---------| ----| --------------|
| Twitter  | TWITTER_CONSUMER_KEY    | - |
| Twitter  | TWITTER_CONSUMER_SECRET | - |
| Twitter  | TWITTER_CALLBACK_URL    | http://localhost:8000/auth/twitter/callback |
| Facebook | FACEBOOK_APP_ID         | -  |
| Facebook | FACEBOOK_APP_SECRET     | -  |
| Facebook | FACEBOOK_CALLBACK_URL   | http://localhost:8000/auth/facebook/callback  |
| Google   | GOOGLE_REALM            | http://localhost:8000  |
| Google   | GOOGLE_RETURN_URL       | http://localhost:8000/auth/google/return |
| LinkedIn | LINKED_IN_KEY           | -  |
| LinkedIn | LINKED_IN_SECRET        | -  |
| LinkedIn |LINKED_IN_CALLBACK_URL   | http://localhost:8000/auth/linkedin/callback |

## Tests
To run automated server tests:
```
npm test
```