var config = require('../config')
    , User =      require('../models/User.js')
    , Promise = require('bluebird')
    , Fb =      Promise.promisifyAll(require('../models/Facebook.js'))

module.exports = {
  profile : function(req, res, next){
    User.getAccessToken(function(err, accesstoken){
      if(err) res.json(err);
      Promise.props({
        profile: Fb.request(accesstoken,"/me"),
        profilepicture : Fb.request(accesstoken,"/me/picture"),
        books : Fb.request(accesstoken, "/me/books")
      }).then(function(result){
        console.log("the books are : ", result.profile);
          return res.json({"profile" : result.profile, "profilepicture" : result.profilepicture, "books" : result.books});
      }, function(err){
          return res.json(err);
      }).catch(function(e) {
          console.log(e);
      });
    });
  }
};
