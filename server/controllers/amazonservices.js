var config = require('../config'),
    amazon = require('amazon-product-api');

function initAWS()
{
  var client = amazon.createClient({
    awsId: config.aws.AWS_ACCESS_KEY_ID,
    awsSecret: config.aws.AWS_SECRET_ACCESS_KEY,
    awsTag: config.aws.AWS_ADVERTISER_TAG
  });
  return client;
}

module.exports = {
  search : function(req, res, next){
    var searchterm  = req.param('searchterm');
    var client  = initAWS();
    client.itemSearch({
      keywords: searchterm,
      searchIndex: 'Books',
      responseGroup: 'ItemAttributes,Offers,Images'
    }, function(err, results) {
      if (err) console.log(err);
      res.json({results:results});
    });
  }
};
