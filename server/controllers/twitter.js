var config = require('../config')
    , User =      require('../models/User.js')
    , Promise = require('bluebird')
    , Twitter =      Promise.promisifyAll(require('../models/Twitter.js'))

module.exports = {
  profile : function(req, res, next){
    User.getAccessToken(function(err, accesstoken){
      var accesstoken, accesssecret;
      if(accesstoken.twitter_token)
        accesstoken = accesstoken.twitter_token;
      if(accesstoken.twitter_secret)
        accesssecret = accesstoken.twitter_secret;
      if(err) res.json(err);
      Promise.props({
        profile: Twitter.request(accesstoken,"/me"),
        profilepicture : Twitter.request(accesstoken,"/me/picture"),
        books : Twitter.request(accesstoken, "/me/books")
      }).then(function(result){
        console.log("the books are : ", result.books);
          return res.json({"profile" : result.profile, "profilepicture" : result.profilepicture, "books" : result.books});
      }, function(err){
          return res.json(err);
      }).catch(function(e) {
          console.log(e);
      });
    });
  }
};
