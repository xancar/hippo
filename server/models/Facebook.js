var graph = require('fbgraph'),
    access_token,
    Parse = require('parse').Parse,
    config = require('../config'),
    ParseUser = Parse.Object.extend("User"),
    User = require('./User'),
    Promise = require('bluebird');
Parse.initialize(config.parse.applicationid, config.parse.javascriptkey);

module.exports = {
  request : function(accesstoken, url){
    var promise = new Promise(function(resolve, reject){
      graph
      .setAccessToken(accesstoken)
      .get(url, function(err, res) {
      if(err) return reject(err)
      return resolve(res); // { image: true, location: "http://profile.ak.fb..." }
      });
    });
    return promise;
  }
};
