var graph = require('twit'),
    access_token,
    Parse = require('parse').Parse,
    config = require('../config'),
    ParseUser = Parse.Object.extend("User"),
    User = require('./User'),
    Promise = require('bluebird');
Parse.initialize(config.parse.applicationid, config.parse.javascriptkey);

module.exports = {
  initialize : function(accesstoken, accesssecret)
  {
    var T = new Twit({
        consumer_key:         config.twitter.TWITTER_API_KEY
      , consumer_secret:      config.twitter.TWITTER_API_SECRET
      , access_token:         accesstoken
      , access_token_secret:  accesssecret
    });
    return T;
  },
  request : function(twitter){
    var promise = new Promise(function(resolve, reject){
      graph
      .setAccessToken(accesstoken)
      .get(url, function(err, res) {
      if(err) return reject(err)
      return resolve(res); // { image: true, location: "http://profile.ak.fb..." }
      });
    });
    return promise;
  }
};
