
var assert = require('assert');

// Load app configuration
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';
// test environment uses development config
if (env === 'test') env = 'development';

exports = module.exports = require('./env/' + env + '.json');

exports.env = env;

// Express port
exports.port = process.env.PORT || 3000;

/*var gpu_key = process.env.GPU_SSH;
if (gpu_key) exports.gpu_ssh_key = new Buffer(require('./fix-ssh-key')(gpu_key));*/
